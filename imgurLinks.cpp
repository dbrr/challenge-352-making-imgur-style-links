
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

/*
	AUTHOR: Daybreak.

	CHALLENGE #352: IMGUR LINKS
	Short links have been all the rage for several years now, spurred in part by Twitter's character limits. 
	Imgur - Reddit's go-to image hosting site - uses a similar style for their links. 
	Monotonically increasing IDs represented in Base62.

	The task is to convert a number to its Base62 representation.	
*/

#define DEBUG 0

void decTo62 (long val) {
	
	string base62 = "0123456789abcdefghijklmnopqrstuvwyxzABCDEFGHIJKLMNOPQRSTUVWYXZ";

	do {
	 	cout << base62[val % 62];
	}
	while(val /= 62);
	cout << endl;

	return;
};


int main(void) {
	
	long inputVal = 0;

	cout << "Converting decimal into base 62" << endl;
	cin >> inputVal;

#if DEBUG
	cout << "DEBUGGING (l42): \n";
	cout << "\tinput value: " << inputVal << endl;
#endif

	if (inputVal < 0) {
		cout << "Value cannot be less than 0\n";
		return 1;
	}
	cout << "Answer = ";
	decTo62(inputVal);

	return 0;
}
