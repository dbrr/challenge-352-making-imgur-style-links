# README #

Original link:
https://www.reddit.com/r/dailyprogrammer/comments/7yyt8e/20180220_challenge_352_easy_making_imgurstyle/

Short links have been all the rage for several years now, spurred in part by Twitter's character limits. 
Imgur - Reddit's go-to image hosting site - uses a similar style for their links. 
Monotonically increasing IDs represented in Base62.

Your task is to convert a number to its Base62 representation.